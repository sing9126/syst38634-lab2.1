package TimeTest;

import static org.junit.Assert.*;

import org.junit.Test;

import Time.Time;

public class TimeTest {

	@Test
	public void testGetTotalSecondsRegular() {
		
		int totalSeconds = Time.getTotalSeconds("02:02:02");
		assertTrue("The time provided do not match the result", totalSeconds == 7322);
	}
	
	@Test
	public void testGetTotalSecondsBoundryIn() {
		
		int totalSeconds = Time.getTotalSeconds("01:01:59");
		assertTrue("The time provided do not match the result", totalSeconds == 3719);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundryOut() {
		
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		assertTrue("The time provided do not match the result", totalSeconds == 3720);
	
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		
		int totalSeconds = Time.getTotalSeconds("01:01:0i");
		assertTrue("The time provided do not match the result", totalSeconds == 3660);
	
	}
}
