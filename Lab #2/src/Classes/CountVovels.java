package Classes;

public class CountVovels {

	
public static void main(String[] args) {
	
	String test = "w3resource";
	System.out.println(countVovels(test));
}	

public static int countVovels(String s) {
	
	int count=0;
	for(int i=0;i<s.length();i++) {
		
		if (s.charAt(i) == 'a' || s.charAt(i) == 'e' || s.charAt(i) == 'i' || s.charAt(i) == 'o' || s.charAt(i) == 'u' ) {
			
			count += 1;
		}
		
	}
	return count;
}
}
