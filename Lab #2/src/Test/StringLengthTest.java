package Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Classes.StringLength;

class StringLengthTest {

	@Test
	void testStringLength() {
		String test = "w3rsource.com";
		int length = StringLength.stringLength(test);
		assertTrue("Result matches:", length == 13);
	}

}
