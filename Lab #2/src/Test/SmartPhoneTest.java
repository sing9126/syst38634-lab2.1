package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import Classes.SmartPhone;
import Classes.VersionNumberException;

public class SmartPhoneTest {

	@Test
	public void testGetFormattedPriceRegular() {
		SmartPhone phone = new SmartPhone("iPhone", 2000.00,1.00);
		assertTrue("invalid format", "$2,000".equals(phone.getFormattedPrice()));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryIn() {
		SmartPhone phone = new SmartPhone("iPhone", 999.00,1.00);
		assertTrue("invalid format", "$999".equals(phone.getFormattedPrice()));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryOut() {
		SmartPhone phone = new SmartPhone("iPhone", 999.10,1.00);
		assertFalse("invalid format", "$999.10".equals(phone.getFormattedPrice()));
	}
	
	@Test
	public void testGetFormattedPriceException() {
		SmartPhone phone = new SmartPhone("iPhone", 1000.8999,1.00);
		assertFalse("invalid format", "$1000.8999".equals(phone.getFormattedPrice()));
	}
	
	
	
	
	
	@Test
	public void testSetVersionRegular() throws VersionNumberException {

		SmartPhone phone = new SmartPhone("iPhone", 1000.00,2.00);
		
		assertTrue("Valid Version", phone.getVersion() == 2.00);
	}
	
	@Test
	public void testSetVersionBoundaryIn() throws VersionNumberException {

		SmartPhone phone = new SmartPhone("iPhone", 1000.00,3.00);
		
		assertTrue("Valid Version", phone.getVersion() == 3.00);
	}

	
	@Test(expected=VersionNumberException.class)
	public void testSetVersionBoundaryOut() throws VersionNumberException {

		SmartPhone phone = new SmartPhone("iPhone", 1000.00,4.10);
		phone.setVersion(4.10);
		assertFalse("Invalid Version", 4.10==phone.getVersion());
	}
	
	@Test(expected=VersionNumberException.class)
	public void testSetVersionException() throws VersionNumberException {

		SmartPhone phone = new SmartPhone("iPhone", 1000.00,4.10);
		phone.setVersion(12.00);
		assertFalse("Invalid Version",12.00==phone.getVersion());
	}
}
