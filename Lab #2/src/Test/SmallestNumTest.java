package Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Classes.SmallestNum;

class SmallestNumTest {

	@Test
	void testFindSmall() {
		int[] testArray = {37,25,29};
		int a = SmallestNum.findSmall(testArray);
		assertTrue("Result matches:", a == 25);
	}

}
