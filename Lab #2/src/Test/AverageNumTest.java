package Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Classes.AverageNum;
import Classes.SmallestNum;

class AverageNumTest {

	@Test
	void testFindAVG() {
		
		int[] testArray = {25,45,65};
		double a = AverageNum.findAVG(testArray);
		assertTrue("Result matches:", a == 45.0);
		
	}

}
