package Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Classes.findChar;

class findCharTest {

	@Test
	void testCharIndex() {
		String s = "Java Exercises!";
		String test = findChar.charIndex(s, 10);
		assertTrue("Result matches:", test.equals("Char is: i"));
		
	}

}
