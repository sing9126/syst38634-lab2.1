package Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Classes.CountVovels;
import Classes.MiddleChar;

class CountVovelsTest {

	@Test
	void testCountVovels() {
		String s ="w3resource";
		int test = CountVovels.countVovels(s);
		assertTrue("Result matches:", test == 4);
		
	}

}
