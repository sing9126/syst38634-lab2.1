package Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Classes.AverageNum;
import Classes.MiddleChar;

class MiddleCharTest {

	@Test
	void testFindMiddleChar() {
		String testString="350";
		String test = MiddleChar.findMiddleChar(testString);
		assertTrue("Result matches:", test.equals("Middle char is:5"));
	}

}
